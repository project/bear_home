About
-----------
Bear Homepage is a features package that provides an exported panel configuration for a custom homepage with a built in Bear Slideshow views content pane for easy setup of a homepage slideshow. The front variable in Site Configuration is also set by strongarm.

Instructions
-----------
View INSTALL.txt for specific instructions.
